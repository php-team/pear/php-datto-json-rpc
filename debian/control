Source: php-datto-json-rpc
Section: php
Priority: optional
Maintainer: Debian PHP PEAR Maintainers <pkg-php-pear@lists.alioth.debian.org>
Uploaders: Fab Stz <fabstz-it@yahoo.fr>, James Valleroy <jvalleroy@mailbox.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-phpcomposer,
               phpab,
               phpunit,
               pkg-php-tools (>= 1.41~)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://www.datto.com/
Vcs-Browser: https://salsa.debian.org/php-team/pear/php-datto-json-rpc
Vcs-Git: https://salsa.debian.org/php-team/pear/php-datto-json-rpc.git

Package: php-datto-json-rpc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}, ${shlibs:Depends}
Recommends: ${misc:Recommends}, ${phpcomposer:Debian-recommend}
Suggests: ${phpcomposer:Debian-suggest}
Conflicts: ${phpcomposer:Debian-conflict}
Provides: ${phpcomposer:Debian-provide}
Description: ${phpcomposer:description}
 This package allows you to create and evaluate JSON-RPC messages, using your
 own PHP code to evaluate the requests.
 .
 This package allows you to create and evaluate any JSON-RPC message. It
 implements the JSON-RPC specifications, but does not provide a transport
 layer—which you'll also need if you want to send or receive messages over
 a distance! One of the beautiful features of JSON-RPC is that you can use
 any transport layer to carry your messages: This package gives you that option.
 .
 Features:
  - Correct: fully compliant with the JSON-RPC 2.0 specifications (100%
    unit-test coverage)
  - Flexible: you can use your own code to evaluate the JSON-RPC method
    strings
  - Minimalistic: extremely lightweight
  - Ready to use, with working examples
